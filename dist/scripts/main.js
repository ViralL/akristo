'use strict';

// ready
$(document).ready(function () {

    //steps
    $(".steps-list").steps({
        headerTag: ".step-title",
        bodyTag: ".step-body",
        transitionEffect: "slideLeft",
        autoFocus: true,
        labels: {
            next: "Следующий шаг",
            finish: "ОФОРМИТЬ ЗАКАЗ"
        }
    });
    //steps

    // anchor
    $(".anchor").on("click", "a", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({ scrollTop: top + 40 }, 1000);
    });
    $(".anchor-on").on("click", "a", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({ scrollTop: top - 120 }, 1000);
        $(id).find('.accordion-body').slideDown(350);
    });
    // anchor

    // adaptive menu
    $('.main-nav__toggle--js').click(function () {
        $(this).next().slideToggle();
    });
    // adaptive menu

    // mask phone {maskedinput}
    $("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    // slider {slick-carousel}
    $('.slider').slick({
        arrows: false,
        dots: true,
        fade: true,
        infinite: true
    });
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        centerMode: true,
        focusOnSelect: true
    });
    // slider

    // select {select2}
    $('select').select2({
        minimumResultsForSearch: Infinity
    });
    // select

    // input[type=file]
    $('.input__file-js').change(function () {
        $('.input__file-js').each(function () {
            var name = this.value;
            var reWin = /.*\\(.*)/;
            var fileTitle = name.replace(reWin, "$1");
            var reUnix = /.*\/(.*)/;
            fileTitle = fileTitle.replace(reUnix, "$1");
            $(this).parent().find('.btn').val(fileTitle);
        });
    });
    // input[type=file]

    //accordion
    $('.accordion-header').click(function (e) {
        e.preventDefault();
        var $this = $(this);
        if ($this.hasClass('show')) {
            $this.removeClass('show');
            $this.next().slideUp(350);
        } else {
            $this.toggleClass('show');
            $this.next().slideToggle(350);
        }
    });

    // popup {magnific-popup}
    $('.popup').magnificPopup({
        closeBtn: false
    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
    // popup

    // mobile sctipts
    var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    if (screen_width <= 767) {}
    // mobile sctipts

    //toTop
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.toTop').fadeIn();
        } else {
            $('.toTop').fadeOut();
        }
    });

    //Click event to scroll to top
    $('.toTop').click(function () {
        $('html, body').animate({ scrollTop: 0 }, 800);
        return false;
    });
    //toTop
});
// ready

//map
ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
        center: [55.751574, 37.573856],
        zoom: 14,
        controls: []
    }),
        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
        hintContent: 'Собственный значок метки',
        balloonContent: 'Это красивая метка'
    }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: 'images/bubble.svg',
        // Размеры метки.
        iconImageSize: [64, 76],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [-5, -38]
    });
    myMap.behaviors.disable('scrollZoom');
    myMap.geoObjects.add(myPlacemark);
});
//map
//# sourceMappingURL=main.js.map
